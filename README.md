# wp
A fun little utility for setting wallpapers. You need to have `feh` installed in order to use it. It's recommended to use this script in conjunction with a window manager that allows you to make keybindings for shell commands
(like XMonad or bspwm) so that you can control your wallpapers from the keyboard.
## Documentation
```
Usage: wp [OPTION] [-m=--bg-fill] [-d=~/Pictures/wallpapers/]

Options:
			-r				 set a random wallpaper
			-c 				 set the wallpaper to the current one
			-n				 set the next wallpaper in the list
			-p				 set the previous wallpaper in the list
		    -d DIR 		     read wallpapers from DIR
			-m MODE		     use the feh mode MODE for setting the background
			-h 				 display this help message
```
